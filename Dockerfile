FROM debian

RUN apt update && apt install --no-install-recommends -y clang-format && apt clean --dry-run

COPY check-style /usr/local/bin/check-style
