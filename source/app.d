import std.stdio;
import std.file;
import std.path;
import std.process;

int main( string[] args ) {
	if ( args.length < 2 ) {
		writeln("Usage: check-style PROJECT_PATH");
		return 1;
	}
	auto projectPath = args[1];

	if ( isFile( projectPath ) ) {
		if ( !isCppFile( projectPath ) ) {
			stderr.writeln( "\"", projectPath, "\" is not a C++ project or file" );
			return 1;
		}
		if ( !checkStyle( projectPath ) ) {
			stderr.writeln( "Invalid style in file \"", projectPath, "\"" );
			return 1;
		}
	} else { 
		foreach ( name; dirEntries( projectPath, SpanMode.depth ) ) {
			if ( !isCppFile( name ) )
				continue;
			if ( !checkStyle( name ) ) {
				stderr.writeln( "Invalid style in file \"", name, "\"" );
				return 1;
			}
		}
	}

	return 0;
}

bool isCppFile( string name ) {
	immutable ext = extension( name );
	if ( ext != ".cpp" && ext != ".cxx" && ext != ".c" && ext != ".hpp" && ext != ".hxx" && ext != ".h" )
		return false;
	return true;
}

bool checkStyle( string name ) {
	immutable cfresult = wait( spawnProcess( ["clang-format", name], stdin, File( name ~ ".fmt", "w" ), stderr, null, Config.none, dirName( name ) ) );
	if ( cfresult != 0 ) {
		stderr.writeln( "Clang format is failed at file \"", name, "\"" );
		remove( name ~ ".fmt" );
		return false;
	}

	if ( !exists( name ~ ".fmt" ) ) {
		stderr.writeln( "Clang format does not create  \"", name, ".fmt\"" );
		return false;
	}

	auto origFile = read( name );
	auto fmtFile = read( name ~ ".fmt" );

	if ( origFile != fmtFile ) {
		wait( spawnProcess( ["diff", name, name ~ ".fmt"] ) );
		remove( name ~ ".fmt" );
		return false;
	}
	remove( name ~ ".fmt" );
	return true;
}
